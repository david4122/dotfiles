# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Add user binaries to PATH
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
	PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

shopt -s globstar

set +f
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		[ -f "$rc" ] && . "$rc"
	done
fi
unset rc

export EDITOR="$(which vim)"
export HISTCONTROL=ignoreboth
