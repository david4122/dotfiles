#!/bin/bash

# export GRADLE_HOME=/opt/gradle/gradle-8.5

gradle() {
	local base=$(pwd)
	until [[ -f "$base/gradlew" ]]; do
		base="$(dirname "$base")"

		if [[ "$base" = "/" ]]; then
			eval "$GRADLE_HOME/bin/gradle" $@
			return
		fi
	done

	echo "Using $base/gradlew..."
	eval "$base/gradlew" $@
}
