#!/bin/bash

remindin() {
	sleep "$1" && notify-send -u critical "$2" && espeak "$2" &
}
