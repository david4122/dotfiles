#/bin/bash

set_style() {
	content="$1"
	shift
	for s in "$@"; do
		printf '\[%s\]' "$s"
	done
	echo -n "$content"
}

git_stat() {
	if branch="$(git branch --show-current 2>/dev/null)"; then
		[[ -z "$branch" ]] && branch="$(git rev-parse --short HEAD)"
		[[ -n "$(git status -s)" ]] && stat="$2"

		printf "%s %s%s" "$1" "$branch" "$stat"
	fi
}

prompt() {
	local last_code="$?"

	local bgc="$(tput setab 233)"
	local b="$(tput bold)"
	local f="$(tput dim)"
	local n='\033[22m'
	local fgc="$(tput setaf 7)"
	local r="$(tput sgr0)"
	local ok="$(tput setaf 3)"
	local err="$(tput setaf 1)"

	# setup sections
	local host
	[ -n "$TOOLBOX_PATH" ] && host="TOOLBOX" || host="$(hostname)"

	local user="$(whoami)"
	local path="$(dirs +0)"
	local sec_host="$(set_style "${user}"$(set_style "@${host}" "$b") "$n" "$fgc" "$bgc")"
	local sec_path="$(set_style "${path} " "$f" "$fgc" "$bgc")"
	local sec_time="$(set_style '\t' "$n" "$fgc" "$bgc")"
	local sec_privil="$(set_style '\$ ' "$b" "$fgc" "$bgc")"

	local sec_git="$(git_stat "" "$(set_style "*" "$b")")"
	[[ -n "$sec_git" ]] && sec_git="$(set_style "$sec_git" $r $n "$bgc") "

	local sec_err
	[[ "$last_code" -ne 0 ]] && sec_err="$(set_style "($last_code) " "$err")"

	local sec_jobs
	local jobs="$(jobs | grep '^\[' | wc -l)"
	[[ "$jobs" -gt '0' ]] && sec_jobs="$(set_style "($jobs)" "${b}${fgc}") "

	local sec_venv
	[[ -n "$VIRTUAL_ENV" ]] && sec_venv=$(set_style "(VE) " "$fgc")

	# setup frame
	local frame_style
	[[ "$last_code" -eq '0' ]] && frame_style="${b}${ok}" || frame_style="${b}${err}"

	local frame="$(set_style ' ┌[ ' "$frame_style" "$bgc")"
	frame+="%s"
	frame+="$(set_style ' ] ' "$frame_style" "$bgc")"
	frame+="%s$(set_style '' "$r")\n"
	frame+="$(set_style ' └[ ' "$frame_style" "$bgc")"
	frame+="%s"
	frame+="$(set_style ' ] ' "$frame_style" "$bgc")"
	frame+="%s$(set_style '' "$r") "

	echo -en "\e]2;${user}@${host}: ${path}\e\\"

	# build prompt
	PS1="$(printf "\n$frame" \
		"$sec_host" \
		"$sec_path" \
		"$sec_time" \
		"${sec_git}${sec_venv}${sec_jobs}${sec_privil}${sec_err}")"
}

PROMPT_COMMAND=prompt

test_colors() {
	for f in {30..37}; do
		for b in {40..47}; do
			printf "\033[%s;%smTEST %s %s\033[0m\t" "$f" "$b" "$f" "$b"
		done
		printf "\n\n"
		for b in {100..107}; do
			printf "\033[%s;%smTEST %s %s\033[0m\t" "$f" "$b" "$f" "$b"
		done
		printf "\n\n"
		for b in {232..255}; do
			printf "\033[48;5;%smTEST %s %s\033[0m\n" "$b" "$f" "$b"
		done
	done
}
