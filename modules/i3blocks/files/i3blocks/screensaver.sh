#!/bin/bash

state="$(xset q | awk '/DPMS is / { print $3 }')"

if [[ -n "$button" ]] && [[ "$button" = "3" ]]; then
	if [[ "$state" = "Enabled" ]]; then
		xset -dpms
		xset s off
	else
		xset +dpms
		xset s off
	fi
	(sleep 0.15s; pkill -SIGRTMIN+10 i3blocks) &
fi

[[ "$state" = "Enabled" ]] && echo "$label_enabled" || echo "$label_disabled"
