#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>

void readBytes(const char* filter, long* rbytes, long* tbytes);

char* reduce(double val, const char** units, int unitc);

int main(int argc, char** argv) {

	setvbuf(stdout, NULL, _IONBF, 0);

	char* iface_filter = argv[1];
	char* r_label = (argc <= 2) ? "R " : argv[2];
	char* t_label = (argc <= 3) ? "T " : argv[3];
	int interval = 5;

	int unitc = 4;
	const char *units[5] = {"b/s", "Kb/s", "Mb/s", "Gb/s"};

	long rbytes = 0, tbytes = 0,
		 old_rbytes = 0, old_tbytes = 0;
	long rbps, tbps;

	readBytes(iface_filter, &old_rbytes, &old_tbytes);

	while(1) {
		sleep(interval);

		readBytes(argv[1], &rbytes, &tbytes);

		rbps = (double)(rbytes - old_rbytes) / interval;
		tbps = (double)(tbytes - old_tbytes) / interval;

		printf("%s %s %s %s\n",
				r_label, reduce(rbps, units, unitc),
				t_label, reduce(tbps, units, unitc));

		old_rbytes = rbytes;
		old_tbytes = tbytes;
	}

	return 0;
}

void readBytes(const char* filter, long* rbytes, long* tbytes) {

	FILE* dev = fopen("/proc/net/dev", "r");

	if(dev == NULL) {
		puts("FAILED TO OPEN");

		(*rbytes) = 0;
		(*tbytes) = 0;

		return;
	}

	// skip header
	fscanf(dev, "%*[^\n]\n");
	fscanf(dev, "%*[^\n]\n");

	char iface[50];
	long rb, tb;
	while(fscanf(dev, "%[^:]: %ld %*s %*s %*s %*s %*s %*s %*s %ld %*[^\n]\n",
				iface, &rb, &tb) != EOF) {

		if(strcmp(iface, filter) == 0) {
			(*rbytes) = rb;
			(*tbytes) = tb;

			break;
		}
	}

	fclose(dev);
}

char* reduce(double val, const char** units, int unitc) {
	int unit_i = 0;
	while(val > 1000 && unit_i < unitc) {
		val /= 1000;
		unit_i++;
	}

	char* buffer = (char*) malloc(sizeof(char) * 100);

	char* unit_pre = "<span size=\"small\">";
	char* unit_post = "</span>";

	sprintf(buffer, "%5.1f%s %-4s%s", val, unit_pre, units[unit_i], unit_post);
	return buffer;
}
