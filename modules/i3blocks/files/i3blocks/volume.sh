#!/bin/bash

sink="$(pactl info | grep 'Default Sink' | cut -d\  -f3)"

step="${2:-4}"

case "$button" in
	3)
		next="$(pactl list sinks | awk '
				NR==1 { first=$2 }
				/Name: '$sink'/ { d=1 }
				/^Sink/ {
					if (d == 1) {
						f=1
						print $2;
						exit;
					}
				}
				END { if (!f) print first }
			' | cut -b2-)"

		pactl set-default-sink "$next"

		inputs=($(pactl list short sink-inputs | cut -f1))
		for input in "${inputs[@]}"; do 
			pactl move-sink-input "$input" "$next"
		done

		(sleep 0.15s; pkill -SIGRTMIN+10 i3blocks) &
		;;
	4)
		pactl set-sink-volume "$sink" +$step%
		;;
	5)
		pactl set-sink-volume "$sink" -$step%
		;;
	2)
		pactl set-sink-mute "$sink" toggle
		;;
esac

volume=($(pactl list sinks | awk '/'$sink'/ { found=1 }
	/Description: / { if(found) { print $2 } }
	/Mute: / { if(found) print $2 }
	/Volume: / { if (found) { print $5; exit } }'))

if [ "${volume[1]}" = "yes" ]; then
	echo "<span foreground=\"$color_muted\">${volume[0]} ${label_muted:-M }(${volume[2]})</span>"
else
	vol_perc="${volume[2]}"
	vol_perc="${vol_perc/\%/}"

	if [ "$vol_perc" -gt "40" ]; then
		label="$label_high"
	elif [ "$vol_perc" -gt "0" ]; then
		label="$label_low"
	else
		label="$label_off"
	fi

	echo "${volume[0]} ${label:-V }${volume[2]}"
	echo "${label:-V }${volume[2]}"
fi
