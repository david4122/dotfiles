#!/bin/bash


if [ -n "$button" ]; then
	step="${2:-5}"

	case "$button" in
		4)
			light -A "$step"
			;;
		5)
			light -U "$step"
			;;
	esac
fi

printf '%.0f%%\n' "$(light -G)"
