#!/bin/bash

if [[ -n "$button" ]] && [[ "$button" -eq "3" ]]; then
	~/bin/toggle_overview >&2>/dev/null
fi

awk '
	/MemTotal:/ { total = $2 }
	/MemAvailable:/ {
		avail = $2
		perc = (total - avail)/total * 100

		printf "%.2fG (%.2f%)\n", avail/1024/1024, perc
		printf "%.2fG\n", avail/1024/1024

		if(perc > 70)
			print "#FF0000"
		else if(perc > 40)
			print "#FFFF00"
		else
			print "#008800"
	}
' /proc/meminfo
