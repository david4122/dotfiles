#!/bin/bash

PING_ADDR="1.1.1.1"

exec 3< <(ping -c1 "$PING_ADDR" 2>/dev/null | awk -f ping.awk)

./netspeed "$iface" "$label_down" "$label_up" | while read speed; do
	# net="$(nmcli device | awk '/^wlp2s0/ { printf "<span size=\"small\">%s</span>\n", $4 }')"
	read <&3 p

	full_escaped="$(echo "$label_net$p ${iface:0:4}: $speed" | sed 's/"/\\"/g')"

	printf '{"full_text": "%s", "short_text": "%s"}\n' "$full_escaped" "$p"

	exec 3< <(ping -c1 "$PING_ADDR" | awk -f ping.awk)
done
