#!/bin/bash

BAT_PATH="/sys/class/power_supply/BAT0"

[[ -d "$BAT_PATH" ]] || exit 0

capacity="$(cat "$BAT_PATH/capacity")"
bat_status="$(cat "$BAT_PATH/status")"

case "$bat_status" in
	Discharging)
		label="${discharging_label:-D }"
		;;
	Charging)
		label="${charging_label:-C }"
		;;
	Full)
		label="${full_label:-F }"
		;;
	Unknown)
		label="${unknown_label:-U }"
		;;
esac

printf '%s%d%%\n' "$label" "$capacity"

[[ "$capacity" -lt "15" ]] && exit 33 || exit 0
