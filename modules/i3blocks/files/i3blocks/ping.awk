#!/usr/bin/awk

BEGIN {
	FS="="
}

/time=/ {
	split($4, arr, " ");
	print arr[1];
}
