#!/bin/bash

if [[ -n "$button" ]] && [[ "$button" -eq "3" ]]; then
	toggle_overview.sh >&2>/dev/null
fi

load="$(cat /proc/loadavg | cut -d\  -f1)"

echo "$load"
echo "$load"

printf -v load_int '%.1f' "$load"
load_int="${load_int/\./}"
./gradient "$load_int" "70" "008000" "FFFF00" "FF0000"
