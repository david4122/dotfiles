#!/bin/bash

dunstctl set-paused toggle

[ "$(dunstctl is-paused)" = "false" ] \
	&& notify-send -u low 'Notifications enabled'
