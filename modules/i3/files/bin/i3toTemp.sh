#/bin/bash

TEMP_PREFIX="t"


filter_wss() {
	i3-msg -t get_workspaces \
		| jq -r --arg p "$1" '.[].name
			| capture("\($p):(?<name>[0-9]+)")
			| .name'
}

last_in_prefix() {
	echo "$(filter_wss $1 | tail -1)"
}

next_in_prefix() {
	last="$(last_in_prefix $1)"
	echo "$((last + 1))"
}

go_to_in_prefix() {
	go_to "${1}:${2}"
}

move_to_in_prefix() {
	move_to "${1}:${2}"
}

go_to() {
	i3-msg -q workspace "$1"
}

move_to() {
	i3-msg -q move to workspace "$1"
}

print_flag() {
	echo -e "\t$1\t$2"
}

print_help() {
	echo "Usage: $0 [OPTIONS]"
	echo "Creates a new temporary i3 workspace and switches to it."
	print_flag "-h" "print this help message"
	print_flag "-p" "pull currently focused window to the temporary workspace"
	print_flag "-l" "go to last temporary workspace"
}

# if [ -z "$1" ]; then
# 	print_help >&2
# 	exit 1
# fi

pull=0
target_w=""

while [ -n "$1" ]; do
	case "$1" in
		-h)
			print_help
			exit
			;;
		-p)
			pull=1
			;;
		-l)
			target_w="$(last_in_prefix "$TEMP_PREFIX")"
			;;
	esac
	shift
done

if [ -z "$target_w" ]; then
	target_w="$(next_in_prefix "$TEMP_PREFIX")"
fi

[ "$pull" = "1" ] && move_to_in_prefix "$TEMP_PREFIX" "$target_w"

go_to_in_prefix "$TEMP_PREFIX" "$target_w"
