[[ -z "$@" ]] && exit 1
i3-msg '[id="'$1'"]' move to workspace current
i3-msg '[id="'$1'"]' focus
