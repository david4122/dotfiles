#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<sys/ioctl.h>


void wait_for(char*, unsigned long);
void parse_args(int argc, char** argv, unsigned long* delay, int* remove, int* rescan, int* help);

int remove_gpu() {
	puts("Removing device...");

	const char* removePath = "/sys/devices/pci0000:00/0000:00:1c.0/remove";
	FILE* remove = fopen(removePath, "w");

	if(NULL == remove) {
		printf("Failed to open %s\n", removePath);
		return -1;
	}

	fprintf(remove, "%d", 1);
	fclose(remove);

	return 0;
}

int pci_rescan() {
	puts("Rescanning PCI");

	const char* rescanPath = "/sys/bus/pci/rescan";
	FILE* rescan = fopen(rescanPath, "w");

	if(NULL == rescan) {
		printf("Failed to open %s\n", rescanPath);
		return -1;
	}

	fprintf(rescan, "%d", 1);
	fclose(rescan);

	return 0;
}

int main(int argc, char** argv) {
	unsigned long delay = 0;
	int remove = 0;
	int rescan = 0;
	int help = 0;

	parse_args(argc, argv, &delay, &remove, &rescan, &help);

	if(help) {
		puts("Usage: reset_gpu [-h] [-d DELAY] [--rm] [--rescan]");
		return 0;
	}

	if(remove && remove_gpu() < 0) {
		puts("Failed to remove device");
		return -1;
	}

	if(delay > 0)
		wait_for("Rescanning in %3d\n", delay);

	if(rescan && pci_rescan() < 0) {
		puts("Failed to rescan");
		return -2;
	}

	return 0;
}

void print_progress_bar(char* msg, unsigned long cur, unsigned long max) {

	struct winsize ws;
	ioctl(1, TIOCGWINSZ, &ws);

	int len = ws.ws_col / 2;
	double curp = (double) cur / max;
	double curLen = curp * len;

	printf("%.1f%% [", curp * 100);

	for(int i = 0; i < curLen; i++)
		printf("=");

	for(int i = curLen; i < len; i++)
		printf(" ");

	printf("] %lu / %lu", cur, max);
}

void wait_for(char* msg, unsigned long secs) {
	printf(msg, secs);
	for(unsigned long sec = 1; sec <= secs; sec++) {
		printf("\r");
		print_progress_bar(msg, sec, secs);
		fflush(stdout);

		sleep(1);
	}
	printf("\n");
}

void parse_args(int argc, char** argv, unsigned long* delay, int* remove, int* rescan, int* help) {
	for(int i = 0; i < argc; i++) {
		if(!strcmp("-d", *(argv + i))) {
			i++;
			*delay = atoi(*(argv + i));
			printf("Delay set to %lu\n", *delay);
		} else if(!strcmp("--rm", *(argv + i))) {
			*remove = 1;
		} else if(!strcmp("--rescan", *(argv + i))) {
			*rescan = 1;
		} else if(!strcmp("--help", *(argv + i)) || !strcmp("-h", *(argv + i))) {
			*help = 1;
		}
	}

	if(!help && !remove && !rescan)
		*remove = *rescan = 1;
}
