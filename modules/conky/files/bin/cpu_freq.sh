#!/bin/bash

cpupower frequency-info | awk '/current CPU frequency: [0-9]/ { print $4$5 }'
