#!/bin/bash

WIDGETS_DIR="$HOME/.config/conky"
WIDGETS=(cpu mem disk top)
I3_MODE=1
CONKY_WS='.'

if ! pgrep conky; then 
	for w in "${WIDGETS[@]}"; do
		[ -f "$WIDGETS_DIR/$w.conkyrc" ] && conky -c "$WIDGETS_DIR/$w.conkyrc"
	done
	exit
fi

if [[ -n "$I3_MODE" ]]; then
	i3-msg -q '[class="Widget" workspace="__focused__"]' sticky disable, move workspace $CONKY_WS 2>/dev/null || \
		i3-msg -q '[class="Widget" workspace="'$CONKY_WS'"]' sticky enable 2>/dev/null
fi
