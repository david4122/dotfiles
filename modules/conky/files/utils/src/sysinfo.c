#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/statfs.h>

#include "sysinfo.h"

int cmpstr(char* s1, char* s2);

/* int main(int argc, char** argv) { */

/* 	double usage; */
/* 	double load; */
/* 	double mem; */

/* 	cpu_usage(&usage); */
/* 	cpu_load(&load); */
/* 	mem_usage(&mem); */

/* 	printf("CPU Usage: %f\n", usage); */
/* 	printf("CPU Load: %f\n", load); */
/* 	printf("Mem Usage: %f\n", mem); */

/* 	return 0; */
/* } */

void cpu_usage(double* usage) {
	FILE* f = fopen("/proc/stat", "r");
	long time = 0, idle = 0, sum = 0;
	char buf[5];

	fscanf(f, "%s", buf);

	for(int i = 0; i < 10; i++) {
		fscanf(f, "%ld", &time);
		if(3 == i)
			idle = time;
		sum += time;
	}

	fclose(f);

	*usage = ((double) idle) / sum;
}

void cpu_load(double* load) {
	long cpus = sysconf(_SC_NPROCESSORS_ONLN);
	double avgs[1];
	getloadavg(avgs, 1);

	*load = avgs[0] / cpus;
}

void mem_usage(double* mem) {
	FILE* f = fopen("/proc/meminfo", "r");
	char buf[20];
	long val;
	char unit[2];

	long total, avail;

	int cnt = 0;
	while(cnt < 2) {
		fscanf(f, "%s", buf);
		fscanf(f, "%ld", &val);
		fscanf(f, "%s", unit);

		if(cmpstr("MemTotal:", buf)) {
			total = val;
			cnt++;
		} else if(cmpstr("MemAvailable:", buf)) {
			avail = val;
			cnt++;
		}
	}

	fclose(f);

	*mem = (((double) total) - avail) / total;
}

void disk_usage(char* disk, double* usage) {
	struct statfs buf;
	statfs(disk, &buf);
	*usage = 1 - ((double) buf.f_bavail / buf.f_blocks);
}

int cmpstr(char* s1, char* s2) {
	while(*s1 == *s2) {
		if(*s1 == '\0')
			return 1;

		s1++;
		s2++;
	}

	return 0;
}
