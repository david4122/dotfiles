#include <stdlib.h>
#include <stdio.h>

#include "colors.h"

color_t* create_color(void) {
	color_t* res = (color_t*) malloc(sizeof(color_t));
	return res;
}

void rgb_to_color(int r, int g, int b, color_t* out) {
	out->red = r;
	out->green = g;
	out->blue = b;
}

void str_to_color(const char* rgb, color_t* out) {
	sscanf(rgb, "%2x%2x%2x", &(out->red), &(out->green), &(out->blue));
}

void color_cpy(const color_t* src, color_t* dest) {
	dest->red = src->red;
	dest->green = src->green;
	dest->blue = src->blue;
}

void color_between(const color_t* s, const color_t* e, double perc, color_t* out) {
	out->red = s->red + (e->red - s->red) * perc;
	out->green = s->green + (e->green - s->green) * perc;
	out->blue = s->blue + (e->blue - s->blue) * perc;
}

void get_color_from_scale(double perc, int colorc, const color_t* colors, color_t* out) {
	if(perc >= 1) {
		color_cpy((colors + colorc - 1), out);

		return;
	}

	double starti = (colorc - 1) * perc;
	double seg_size = 100.0 / colorc;
	const color_t* startc = (colors + (int) starti);
	double p = starti - ((int) (starti));

	color_between(startc, startc + 1, p, out);
}

void print_color(const color_t* c) {
	printf("%02x%02x%02x", c->red, c->green, c->blue);
}

/* int main(int argc, char* argv[]) { */

/* 	if(argc < 4) { */
/* 		fprintf(stderr, "Usage: %s PERCENT START [WAYPOINTS] END\n", argv[0]); */
/* 		return 1; */
/* 	} */

/* 	double perc; */
/* 	sscanf(argv[1], "%lf", &perc); */

/* 	color_t* colors = malloc(sizeof(color_t) * (argc - 2)); */
/* 	for(int i = 2; i < argc; i++) { */
/* 		parse_color(argv[i], (colors + i - 2)); */
/* 	} */

/* 	color_t res; */
/* 	get_color_from_scale(perc, argc - 2, colors, &res); */
/* 	print_color(&res); */

/* 	return 0; */
/* } */
