#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "colors.h"
#include "sysinfo.h"

int main(int argc, char* argv[]) {

	if(argc < 4) {
		fprintf(stderr, "Usage: %s READ_TYPE START_COLOR [WAYPOINT_COLORS] END_COLOR", argv[0]);
		return 1;
	}

	double read;
	int coffset = 2;

	if(!strcmp("cpu", argv[1])) {
		cpu_usage(&read);
	} else if(!strcmp("load", argv[1])) {
		cpu_load(&read);
		read /= 2;
	} else if(!strcmp("mem", argv[1])) {
		mem_usage(&read);
	} else if(!strcmp("disk", argv[1])) {
		disk_usage(argv[2], &read);
		coffset++;
	} else {
		fprintf(stderr, "Invalid type: %s. Has to be one of [cpu|load|mem]", argv[1]);
		return 2;
	}

	color_t* c = create_color();

	color_t* colors = (color_t*) malloc(sizeof(color_t) * (argc - 2));
	for(int i = coffset; i < argc; i++)
		str_to_color(argv[i], (colors + i - coffset));

	get_color_from_scale(read, argc - 2, colors, c);

	/* printf("${color #"); */
	print_color(c);
	/* printf("}"); */

	return 0;
}
