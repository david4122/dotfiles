
#pragma once

struct rgb_color {
	int red;
	int green;
	int blue;
};

typedef struct rgb_color color_t;

color_t* create_color(void);
void rgb_to_color(int r, int g, int b, color_t* out);
void str_to_color(const char* rgb, color_t* out);
void color_between(const color_t* start, const color_t* end, double perc, color_t* out);
void print_color(const color_t* col);

void get_color_from_scale(double perc, int scalec, const color_t* colors, color_t* out);
