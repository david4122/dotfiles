
#pragma once

void cpu_usage(double* usage);

void cpu_load(double* load);

void mem_usage(double* mem);

void disk_usage(char* disk, double* usage);
