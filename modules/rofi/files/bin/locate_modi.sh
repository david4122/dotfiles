#!/bin/bash

RESULTS=100

if [ -n "$1" ]; then

	if [[ -f "$ROFI_INFO" || -d "$ROFI_INFO" ]]; then
		i3-msg -q exec "nautilus \"$ROFI_INFO\""
	else

		locate -i "$1" | head -"$RESULTS" | while read fpath; do \
			path_tail=$(echo "$fpath" | grep -oe ".\{1,50\}$")
			echo -en "$path_tail\0meta\x1f$fpath\x1finfo\x1f$fpath\n"
		done

	fi
fi
