#!/bin/python3

from i3ipc import Connection
from sys import argv

i3 = Connection()

if len(argv) == 1:
    active = -1
    urgent = []

    index = 0
    for workspace in i3.get_workspaces():
        print(f"{workspace.ipc_data['name']}")

        if workspace.ipc_data['urgent']:
            urgent.append(f"{index}")

        if workspace.ipc_data['focused']:
            active = index

        index += 1

    print(f"\0active\x1f{active}")

    if len(urgent) > 0:
        urgent = ','.join(urgent)
        print(f"\0urgent\x1f{urgent}")

elif len(argv) == 2:
    i3.command(f"workspace {argv[1]}")
