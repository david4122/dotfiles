#/bin/bash

if [ -n "$1" ]; then
	arr=($1)
	sink="${arr[0]}"
	pactl set-default-sink "$sink"

	inputs=($(pactl list short sink-inputs | cut -f1))
	for input in "${inputs[@]}"; do 
		pactl move-sink-input "$input" "$sink"
	done

	exit 0
fi

sink="$(pactl info | grep 'Default Sink' | cut -d\  -f3)"

pactl list sinks | awk '
	/^Sink/ { sink=$2; d=0 }
	/Name: '$sink'/ { d=1 }
	/Mute:/ { state=$2 }
	/Description:/ { 
		$1=""
		desc=$0
	}
	/Active Port:/ {
		if($3 == "headset-output") {
			icon="audio-headphones-symbolic"
		} else if($3 == "analog-output-speaker") {
			icon="audio-speakers-symbolic"
		} else {
			icon="audio-card-symbolic"
		}
		printf "%s%s %s %s\0icon\x1f%s\n", sink, desc,
			(state == "yes") ? "(muted)" : "",
			(d == 1) ? "(default)" : "", icon
	}
'
