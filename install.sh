#!/bin/bash

ROOT_PATH="$PWD"
MODULE_PATH="${ROOT_PATH}/modules"
BACKUP_PATH="$HOME/dotfiles_backup"
HOME_PATH="$HOME"
ETC_PATH="/etc"

backup() {
	if [ -e "$1" ]; then
		local back_path="$BACKUP_PATH/$(dirname "$1")"
		mkdir -p "$back_path"
		echo "Backing up $1 to $back_path"
		mv "$1" "$back_path"
	fi
}

link_files() {
	local src=$1
	local dest=$2

	(cd "$src" && find -H . -mindepth 1 -type l -printf "%P\n") \
		| while read lnk; do
			mkdir -p "$(dirname "$dest/$lnk")"
			backup "$dest/$lnk"
			rm -f "$dest/$lnk"
			ln -sv "$src/$lnk" "$dest/$lnk"
		done
}

install_module() {
	echo "Installing module $mod..."

	local mod_home="$1/home"
	local mod_etc="$1/etc"

	[[ -e "$mod_home" ]] && link_files "$mod_home" "$HOME_PATH"
	[[ -e "$mod_etc" ]] && link_files "$mod_etc" "$ETC_PATH"
}

install_mods() {
	for mod in "$@"; do
		install_module "$MODULE_PATH/$mod"
	done
}

if [ -z "$@" ]; then
	echo "Usage: $0 MODULES" >&2
	exit 1;
fi

install_mods "$@"
